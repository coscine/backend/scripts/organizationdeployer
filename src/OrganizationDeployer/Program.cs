﻿using Coscine.Configuration;
using Coscine.Metadata;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using VDS.RDF;
using VDS.RDF.Storage;

namespace Coscine.OrganizationDeployer
{
    public class Program
    {
        public static void Main(string[] args)
        {
            ExecuteCommand(
                "powershell.exe",
                $@"& '{ Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "getOrganizations.ps1") }'"
            );

            var configuration = new ConsulConfiguration();

            var virtuosoServer = configuration.GetString("coscine/local/virtuoso/additional/url");
            var virtuosoHost = new Uri(virtuosoServer).Host;
            var virtuosoUser = configuration.GetString("coscine/global/virtuoso_db_user");
            var virtuosoPassword = configuration.GetString("coscine/global/virtuoso_db_password");

            VirtuosoManager virtuosoManager = new VirtuosoManager($"Server={virtuosoHost};Uid={virtuosoUser};pwd={virtuosoPassword}");

            var _util = new Util(virtuosoServer);
            var folder = "/voc";
            var virtuosoISQLLocation = configuration.GetString(
                "coscine/local/virtuoso/isql",
                "C:/Programs/Virtuoso/bin/isql.exe"
            );
            var queries = new List<string>();
            foreach (var file in GetFiles(folder).Where((file) => file.Contains(".ttl")))
            {
                var fileInfo = new FileInfo(file);
                var graph = new Graph();
                graph.LoadFromFile(file);
                var graphName = graph.BaseUri.ToString();

                if (_util.HasGraph(graphName))
                {
                    Console.WriteLine($"Clearing {graphName}");
                    _util.ClearGraph(graphName);
                }

                queries.Add($"ld_dir('{fileInfo.DirectoryName.Substring(2).Replace("\\", "/")}', '{fileInfo.Name}', '{graphName}');");
            }
            queries.Add($"rdf_loader_run ();");
            queries.Add($"DELETE from DB.DBA.load_list where 1=1;");

            foreach (var query in queries)
            {
                ExecuteCommand(
                    "powershell.exe",
                    $"\"\\\"{query}\\\" | {virtuosoISQLLocation}\""
                );
            }

            Console.WriteLine("Done");
        }

        private static void ExecuteCommand(string fileName, string arguments)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo
            {
                FileName = fileName,
                Arguments = arguments,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false,
                CreateNoWindow = true,
            };
            using (var process = new Process
            {
                StartInfo = startInfo
            })
            {
                process.Start();

                string output = process.StandardOutput.ReadToEnd();
                Console.WriteLine(output);

                string errors = process.StandardError.ReadToEnd();
                Console.WriteLine(errors);
            }
        }


        private static IEnumerable<string> GetFiles(string path)
        {
            Queue<string> queue = new Queue<string>();
            queue.Enqueue(path);
            while (queue.Count > 0)
            {
                path = queue.Dequeue();
                try
                {
                    foreach (string subDir in Directory.GetDirectories(path))
                    {
                        queue.Enqueue(subDir);
                    }
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine(ex);
                }
                string[] files = null;
                try
                {
                    files = Directory.GetFiles(path);
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine(ex);
                }
                if (files != null)
                {
                    for (int i = 0; i < files.Length; i++)
                    {
                        yield return files[i];
                    }
                }
            }
        }
    }
}
