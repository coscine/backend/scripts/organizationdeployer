﻿$extractPath = "C:\Programs\Consul\"
$fileName = "Consul.exe"
$fullPath = ($extractPath + $fileName)

$gitlab_token = & $fullPath kv get "coscine/global/gitlabtoken"

Push-Location $PSScriptRoot

$organizationsPath = 'organizations'
# Twice remove, because of: https://stackoverflow.com/questions/7909167/how-to-quietly-remove-a-directory-with-content-in-powershell#comment10316056_7909195
If(Test-Path $organizationsPath) { Remove-Item -LiteralPath $organizationsPath -Force -Recurse }
If(Test-Path $organizationsPath) { Remove-Item -LiteralPath $organizationsPath -Force -Recurse }

git clone https://gitlab-ci-token:$gitlab_token@git.rwth-aachen.de/coscine/organizations.git

cd organizations

$pagebranch = & $fullPath kv get 'coscine/local/organizationdeployer/branch'

if ($pagebranch -And $pagebranch -ne 'master' ) {
	git checkout $pagebranch
	git pull
}

cd ..

$path = '/voc'

# Twice remove, because of: https://stackoverflow.com/questions/7909167/how-to-quietly-remove-a-directory-with-content-in-powershell#comment10316056_7909195
If(Test-Path $path) { Remove-Item -LiteralPath $path -Force -Recurse }
If(Test-Path $path) { Remove-Item -LiteralPath $path -Force -Recurse }

New-Item -ItemType Directory -Force -Path $path

Copy-Item './organizations/*' $path -Recurse;

Pop-Location